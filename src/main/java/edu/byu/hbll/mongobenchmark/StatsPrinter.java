/**
 * 
 */
package edu.byu.hbll.mongobenchmark;

import java.util.concurrent.CountDownLatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import edu.byu.hbll.stats.time.Times;

/**
 *
 *
 * @author Charles Draper
 */
public class StatsPrinter implements Runnable, AutoCloseable {

  static final Logger logger = LoggerFactory.getLogger(MongoBenchmark.class);

  private Times<?> t;

  private volatile boolean done;

  private CountDownLatch latch = new CountDownLatch(1);

  /**
   * @param t
   */
  public StatsPrinter(Times<?> t) {
    this.t = t;
    new Thread(this).start();
  }

  /**
   * 
   */
  @Override
  public void run() {
    while (!done) {
      try {
        Thread.sleep(10000);
      } catch (InterruptedException e) {
        logger.info(t.toString());
        break;
      }

      logger.info(t.toString());
    }

    latch.countDown();
  }

  /**
   * 
   */
  @Override
  public void close() {
    done = true;

    try {
      latch.await();
    } catch (InterruptedException e) {
    }
  }

}
