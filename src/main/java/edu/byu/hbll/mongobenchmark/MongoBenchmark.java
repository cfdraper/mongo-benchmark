/**
 * 
 */
package edu.byu.hbll.mongobenchmark;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import edu.byu.hbll.stats.time.Benchmark;
import edu.byu.hbll.stats.time.Times;

/**
 * 
 * 
 */
public class MongoBenchmark {

  static final Logger logger = LoggerFactory.getLogger(MongoBenchmark.class);

  private static String DATABASE = "testdb";
  private static String COLLECTION = "documents";
  private static String CHARACTER_POOL = "abcdefghijklmnopqrstuvwxyz";
  private static int UNIQUE_WORDS = 10000;
  private static int DOCUMENTS = 4000000;
  private static Duration TEST_DURATION = Duration.ofMinutes(1);

  public static void main(String[] args) throws Exception {
    if (args.length >= 1 && args[0].equals("build")) {
      build(DATABASE, COLLECTION);
    } else {
      benchmark(DATABASE, COLLECTION);
    }
  }

  public static void build(String databaseName, String collectionName) {

    try (MongoClient mongo = new MongoClient(new MongoClientURI("mongodb://localhost/"))) {

      logger.info("Building " + databaseName + "." + collectionName + " with random documents");

      MongoCollection<Document> c = mongo.getDatabase(databaseName).getCollection(collectionName);

      logger.info("Dropping existing collection");
      c.drop();
      Random r = new Random(4606272284325432744l);

      Set<String> wordSet = new HashSet<>();

      // create a list of random words
      while (wordSet.size() < UNIQUE_WORDS) {
        int length = r.nextInt(10) + 1;
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < length; i++) {
          builder.append(CHARACTER_POOL.charAt(r.nextInt(CHARACTER_POOL.length())));
        }

        wordSet.add(builder.toString());
      }

      List<String> words = new ArrayList<>(wordSet);

      for (int i = 0; i < DOCUMENTS; i++) {

        Document doc = new Document("_id", i);

        for (int j = 0; j < 100; j++) {
          if (r.nextBoolean() || r.nextBoolean()) {

            int fieldLength = r.nextInt(50);
            StringBuilder builder = new StringBuilder();

            for (int k = 0; k < fieldLength; k++) {
              builder.append(words.get(r.nextInt(words.size())));
              builder.append(" ");
            }

            doc.put("field" + j, builder.toString().trim());
          }
        }

        c.insertOne(doc);

        if ((i + 1) % 1000 == 0 || i + 1 == DOCUMENTS) {
          logger.info((i + 1) + " inserted");
        }
      }

      logger.info("Done inserting documents");
    }

  }

  public static void benchmark(String databaseName, String collectionName) throws Exception {

    logger.info("Benchmarking " + databaseName + "." + collectionName);

    try (MongoClient mongo = new MongoClient(new MongoClientURI("mongodb://localhost/"))) {
      MongoCollection<Document> c = mongo.getDatabase(databaseName).getCollection(collectionName);

      List<Object> ids = new ArrayList<>();

      logger.info("Gathering ids");

      for (Document document : c.find().projection(new Document("_id", 1))) {
        ids.add(document.get("_id"));
      }

      logger.info(ids.size() + " documents");

      logger.info("Requesting documents randomly at varying thread counts and batch sizes");

      int[] threadCounts = new int[] {1, 2, 4, 8};
      int[] batchSizes = new int[] {1, 10, 100, 1000};

      // int[] threadCounts = new int[] {1};
      // int[] batchSizes = new int[] {10};

      LocalDateTime start = LocalDateTime.now();

      double[][] results = new double[threadCounts.length][batchSizes.length];

      for (int i = 0; i < threadCounts.length; i++) {
        for (int j = 0; j < batchSizes.length; j++) {

          int threadCount = threadCounts[i];
          int batchSize = batchSizes[j];

          logger.info("Starting thread count " + threadCount + ", batch size " + batchSize);

          Times<Benchmark> t = Times.benchmark();

          try (StatsPrinter printer = new StatsPrinter(t)) {
            ExecutorService executor = null;

            try {
              executor = Executors.newFixedThreadPool(threadCount);

              for (int k = 0; k < threadCount; k++) {
                executor.submit(new Finder(c, ids, batchSize, t, TEST_DURATION));
              }

            } finally {
              if (executor != null) {
                executor.shutdown();
                while (!executor.awaitTermination(1, TimeUnit.DAYS));
              }
            }
          }

          logger.info("Done thread count " + threadCount + ", batch size " + batchSize);
          Benchmark.Data data = t.getStatistic().snapshot();
          logger.info("Throughput : " + ((float) data.getThroughput()) + " documents per second");

          results[i][j] = data.getThroughput();
        }
      }

      StringBuilder builder = new StringBuilder(LocalDateTime.now().toString());

      for (int i = 0; i < threadCounts.length; i++) {
        builder.append("\t");
        builder.append(threadCounts[i]);
      }

      builder.append("\n");

      for (int j = 0; j < batchSizes.length; j++) {
        builder.append(batchSizes[j]);

        for (int i = 0; i < threadCounts.length; i++) {
          builder.append("\t" + results[i][j]);
        }

        builder.append("\n");
      }

      logger.info(
          "Results (columns = thread count, rows = batch size)\n" + builder.toString().trim());

      logger.info("THE10\t" + start.toString().replaceAll("T", " ") + "\t" + results[0][0]);
    }
  }

  @Deprecated
  public static void buildOld(String name, boolean updates) {

    try (MongoClient mongo = new MongoClient()) {

      System.out.println(name);

      MongoCollection<Document> documents = mongo.getDatabase(DATABASE).getCollection(name);
      documents.drop();
      Random r = new Random(4606272284325432744l);

      Set<String> wordSet = new HashSet<>();

      // create a list of random words
      while (wordSet.size() < UNIQUE_WORDS) {
        int length = r.nextInt(10) + 1;
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < length; i++) {
          builder.append(CHARACTER_POOL.charAt(r.nextInt(CHARACTER_POOL.length())));
        }

        wordSet.add(builder.toString());
      }

      List<String> words = new ArrayList<>(wordSet);

      for (int u = 0; u < (updates ? 2 : 1); u++) {
        for (int i = 0; i < DOCUMENTS; i++) {

          int id = u == 0 ? i : r.nextInt(DOCUMENTS);

          Document doc = new Document("_id", id);

          for (int j = 0; j < 100; j++) {
            if (r.nextBoolean() || r.nextBoolean()) {

              int fieldLength = r.nextInt(50);
              StringBuilder builder = new StringBuilder();

              for (int k = 0; k < fieldLength; k++) {
                builder.append(words.get(r.nextInt(words.size())));
                builder.append(" ");
              }

              doc.put("field" + j, builder.toString().trim());
            }
          }

          documents.replaceOne(new Document("_id", id), doc, new UpdateOptions().upsert(true));

          if (i % 1000 == 0) {
            System.out.println(u + ":" + i);
          }
        }
      }
    }

  }

  @Deprecated
  public static Duration benchmark(String name) throws Exception {
    logger.info("Benchmarking " + name);

    // logger.info("Clearing memory cache and restarting Mongo");
    // reset();

    try (MongoClient mongo = new MongoClient()) {
      MongoCollection<Document> documents = mongo.getDatabase(DATABASE).getCollection(name);

      logger.info("Start");
      Instant start = Instant.now();
      documents.count(new Document("fakekey", "fakevalue"));
      Instant end = Instant.now();
      logger.info("End");

      Duration elapsed = Duration.between(start, end);
      logger.info("Elapsed time " + elapsed);

      return elapsed;
    }

  }

  @Deprecated
  public static void shuffle(String name) {
    logger.info("Shuffling " + name);

    try (MongoClient mongo = new MongoClient()) {
      MongoCollection<Document> documents = mongo.getDatabase(DATABASE).getCollection(name);

      List<Object> ids = new ArrayList<>();

      logger.info("Gathering ids");
      for (Document document : documents.find().projection(new Document("_id", 1))) {
        ids.add(document.get("_id"));
      }

      logger.info(ids.size() + " documents to shuffle");

      Collections.shuffle(ids);

      logger.info("Shuffling documents");

      int total = 0;

      for (Object id : ids) {
        Document document = documents.find(new Document("_id", id)).first();

        if (document != null) {
          documents.replaceOne(new Document("_id", id), document);
        }

        if (total++ % 1000 == 0) {
          logger.info("Shuffled " + total + " of " + ids.size() + " documents");
        }
      }

      logger.info("Done shuffling documents");
    }
  }

  @Deprecated
  public static void reset() throws Exception {
    Process p = Runtime.getRuntime().exec("sudo sh bin/reset.sh");
    p.waitFor();
  }

  @Deprecated
  public static void benchmark() throws Exception {
    Duration sequentialTotal = Duration.ZERO;
    Duration fragmentedTotal = Duration.ZERO;

    for (int i = 0; i < 3; i++) {
      sequentialTotal = sequentialTotal.plus(benchmark("sequential"));
      fragmentedTotal = fragmentedTotal.plus(benchmark("fragmented"));
    }

    Duration sequentialAverage = sequentialTotal.dividedBy(3);
    Duration fragmentedAverage = fragmentedTotal.dividedBy(3);

    System.out.println(sequentialAverage);
    System.out.println(fragmentedAverage);
  }

}
