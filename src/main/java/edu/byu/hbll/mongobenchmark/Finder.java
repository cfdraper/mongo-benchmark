/**
 * 
 */
package edu.byu.hbll.mongobenchmark;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mongodb.Block;
import com.mongodb.client.MongoCollection;
import edu.byu.hbll.stats.time.Benchmark;
import edu.byu.hbll.stats.time.Times;

/**
 *
 *
 * @author Charles Draper
 */
public class Finder implements Runnable {

  static final Logger logger = LoggerFactory.getLogger(Finder.class);

  private MongoCollection<Document> c;
  private List<Object> ids;
  private int batchSize;
  private Times<Benchmark> t;
  private Duration test;

  /**
   * @param c
   * @param ids
   * @param batchSize
   * @param t
   * @param test
   */
  public Finder(MongoCollection<Document> c, List<Object> ids, int batchSize, Times<Benchmark> t,
      Duration test) {
    this.c = c;
    this.ids = ids;
    this.batchSize = batchSize;
    this.t = t;
    this.test = test;
  }

  /**
   * 
   */
  @Override
  public void run() {

    Instant start = Instant.now();
    Random r = new Random();

    while (start.plus(test).isAfter(Instant.now())) {

      List<Object> batch = new ArrayList<>();

      for (int i = 0; i < batchSize; i++) {
        batch.add(ids.get(r.nextInt(ids.size())));
      }

      logger.trace("{}", batch);

      Document query = new Document("_id", new Document("$in", batch));

      c.find(query).forEach((Block<Document>) d -> t.mark("find"));

    }

  }

}
